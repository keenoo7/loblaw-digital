
// declare empty object(object app)
let joeFresh = {};

// Empty array for data results
let resultsArray = [];

// Empty array for sorted by price Low to High
let priceArray = [];

// Empty array for sorted by name A - Z
let nameArray = [];

// Empty array for activeArray
let activeArray = [];

// JSON Url
joeFresh.apiUrl = 'https://joefresh-marketing-dev.s3.amazonaws.com/developer-interview/full-list.json';
	
/*
* Init Function
*/
joeFresh.init = function() {
	// Get Data Function
	joeFresh.getData();
};

/*
* Get Data Function
*/
joeFresh.getData = function () {
	$.ajax({
		url: joeFresh.apiUrl,
		method: 'GET',
		dataType: 'json'
	})
	.then(function(data) {
		// Send data to results array
		// This array will be sent through filter functions on change of select box
		resultsArray = data.results;

		// Run displayProducts function on page load with resultsArray
		joeFresh.displayProducts(resultsArray, 20, 'true');
	});
};

/*
* Sort by Price Function
*/
joeFresh.priceArraySort = function(products) {

	// Copy results array
	priceArray = products.slice();

	// Sort array by price
	let priceSorted = priceArray.sort(function(x,y){ return x.productPrice - y.productPrice});

	// Redeclare priceArray with new sorted array
	priceArray = priceSorted;

	// Display products with newly sorted array
	joeFresh.displayProducts(priceArray, 20, 'true');
};

/*
* Sort by Name Function
*/
joeFresh.nameArraySort = function(products) {

	// Copy results array
	nameArray = products.slice();

	// Sort array by name
	let nameSorted = nameArray.sort(function(a, b) {
				    let textA = a.productName.toUpperCase();
				    let textB = b.productName.toUpperCase();
				    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
				});

	// Redeclare nameArray with new sorted array
	nameArray = nameSorted;

	// Display products with newly sorted array
	joeFresh.displayProducts(nameArray, 20, 'true');

};


/*
* Display Products Function
* Takes 3 params: 
* products {array} - array to be displayed
* output {integer} - loop iteration limit (set to 20, or 8 on load more)
* flag {string} - flag to set if emptying of container is needed
*/
joeFresh.displayProducts = function(products, output, flag) {

	// Copy array and declare as activeArray
	activeArray = products.slice();

	// If flag set to true, empty/hide container
	if (flag === 'true') {
		$('.row').fadeOut(200).empty().hide();
		$('.loading-overlay').show();
	}

	// For each product
	for (let i = 0; i < output; i++) {

		// Each time loop is run, it uses first object in array, and then removes object at 
		// end of the loop 
    	let product = activeArray[0];

    	// Declare variable for product badge
    	let badge = '';

    	// Product Image
    	let image = '<figure><img src="' + product.thumbnails.b2 + '"></figure>';

    	// Product Price
    	let price = '<p class="price">$' + product.productPrice + '.00</p>';

    	// If product has badge
    	if (product.productBadgeString) {
    		badge = '<p class="badge">' + product.productBadgeString + '</p>';
    	}

    	// Product Color
    	let color = '<p class="color">' + product.productColor + '</p>';

    	// Product Name
    	let name = '<p class="title">' + product.productName + '</p>';

    	// Append each variable to build product item
    	let itemOutput = $('<div>').addClass('item col-xs-12 col-sm-6 col-md-4 col-lg-3').append(image, price, badge, color, name);

    	// Append each product to container
    	$('.row').append(itemOutput);

    	// Remove this item from array
    	activeArray.shift();

	}

	// If flag is true, fadein container and hide loading overlay
	if (flag === "true") {
		$('.row').fadeIn(200);
		$('.loading-overlay').hide();
	}

};

/*
* Filter Selection Function
*/
joeFresh.filterSelection = function() {
	// On change of filter
	$('.filter').on('change', function () {
		// Get selection value
		let selection = $(this).val();

		// Run sort function based on select value
		if(selection === 'price-low') {
			// Check if the array has already been made
			if (priceArray.length > 0) {
				// reuse array to display products
				joeFresh.displayProducts(priceArray, 20, 'true');
			} else {
				// Run results array through price sort function
				joeFresh.priceArraySort(resultsArray);
			}

		} else if (selection === 'a-z') {
			// Check if the array has already been made
			if (nameArray.length > 0) {
				// reuse array to display products
				joeFresh.displayProducts(nameArray, 20, 'true');
			} else {
				// Run results array through name sort function
				joeFresh.nameArraySort(resultsArray);
			}

		} else if(selection === 'default') {
			// Show default order with resultsArray
			joeFresh.displayProducts(resultsArray, 20, 'true');
		} 

	});

};

/*
* Load More Function
*/
joeFresh.loadMoreFunction = function() {

	// On click of Load More
	$('.load-more').on('click', function () {

		// If more than 8 items left in array
		if(activeArray.length > 8) {

			// Run display products
			joeFresh.displayProducts(activeArray, 8, 'false');

		} else {

			// If less than 8 items left in array
			// Get length of array and set loop iteration
			let lastItems = activeArray.length;
			joeFresh.displayProducts(activeArray, lastItems, 'false');
		}

	});
};

// Document Ready
$(document).ready(function(){
	// Joe Fresh App Init
 	joeFresh.init();
 	// Filter Selection Function
 	joeFresh.filterSelection();
 	// Load More Function
 	joeFresh.loadMoreFunction();
});
